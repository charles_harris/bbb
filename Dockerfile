FROM ubuntu:xenial

ENV HOSTNAME=bbb.dbtune.io

COPY . /

RUN apt update \
    && apt-get -y install \
        dbus \
    && apt-get -y --no-install-recommends install \
        apt-utils \
        ca-certificates \
        lsb-release \
        lsb-core \
        wget \
        sudo \
        net-tools

RUN chmod +x /install.sh \
    && ./install.sh -v xenial-22 -s ${HOSTNAME}

EXPOSE 80
EXPOSE 443
EXPOSE 16384-32768